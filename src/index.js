// @flow

import React from 'react';
import ReactDOM from 'react-dom';

import { createStore, compose, applyMiddleware } from 'redux';
import { connect, Provider } from 'react-redux';

import { Client as tsclient } from './client';

import moment from 'moment';
import momentDurationFormatSetup  from 'moment-duration-format';
import './index.css';


momentDurationFormatSetup(moment);


type TimesheetItem = {
  isIdle: Boolean,
  categories: Array<String>,
  time_started: String
};
type AppState = {
  currenttime: Date,
  lastitem: ?TimesheetItem,
  user: ?Object,
  usercategories: Array<String>,
  userloading: Boolean,
  selectedcategories: Set<String>,
  noitemloaded: Boolean,
  editoropen: Boolean,
  saving: Boolean,
  error: ?Error
};
type AppAction = {};
function appReducer(
  state : AppState = {
    currenttime: new Date(),
    lastitem: {
      isIdle: true,
      categories: [],
      selectedcategories: new Set(),
      time_started: new Date()
    },
    user: {},
    usercategories: [],
    userloading: false,
    noitemloaded: true,
    editoropen: false,
    saving: false,
    error: null
  },
  action : AppAction
) {
  switch (action.type)
  {
    case 'FETCH_USER':
      tsclient.getUser().then(
        user => store.dispatch({ type: 'FETCH_USER_SUCCESS', user })
      ).catch(
        error => store.dispatch({ type: 'FETCH_USER_ERROR', error })
      );
      return {
        ...state,
        saving: action.saving ? false : state.saving,
        editoropen: action.saving ? false : state.editoropen,
        userloading: true
      };
    
    case 'FETCH_USER_SUCCESS':
      return {
        ...state,
        userloading: false,
        user: action.user,
        usercategories: action.user.categories.sort(
          (a, b) => a.order < b.order
        )
      };

    case 'FETCH_USER_ERROR':
    case 'FETCH_LAST_ITEM_ERROR':
    case 'CHANGE_TASK_ERROR':
    case 'SET_CATEGORIES_ERROR':
      setTimeout(
        () => store.dispatch({ type: 'CLEAR_ERROR' }),
        3000
      );
      return {
        ...state,
        userloading: false,
        saving: false,
        error: action.error
      };
    
    case 'CLEAR_ERROR':
      return {
        ...state,
        error: null
      };

    case 'SET_TIME':
      return {
        ...state,
        currenttime: action.time
      }

    case 'FETCH_LAST_ITEM':
      tsclient.getLastItem().then(
        lastitem => store.dispatch({ type: 'FETCH_LAST_ITEM_SUCCESS', lastitem })
      ).catch(
        error => store.dispatch({ type: 'FETCH_LAST_ITEM_ERROR', error })
      );
      return state;
    
    case 'FETCH_LAST_ITEM_SUCCESS':
      const { lastitem } = action;
      lastitem.isIdle = !lastitem.categories.length || (lastitem.categories.length === 1 && lastitem.categories[0] === 'END');
      return {
        ...state,
        noitemloaded: false,
        lastitem
      };

    case 'TOGGLE_SELECTED_CATEGORY':
      const selectedcategories = new Set(state.selectedcategories);
      if (selectedcategories.has(action.category))
      {
        selectedcategories.delete(action.category);
      }
      else
      {
        selectedcategories.add(action.category);
      }
      // console.log('selected categories', selectedcategories);
      return {
        ...state,
        selectedcategories
      }
    
    case 'CLEAR_SELECTED_CATEGORIES':
      return {
        ...state,
        selectedcategories: new Set()
      };
    
    case 'CHANGE_TASK':
      tsclient.putTimesheetItem(action.categories).then(
        lastitem => {
          store.dispatch({ type: 'FETCH_LAST_ITEM_SUCCESS', lastitem });
          store.dispatch({ type: 'CLEAR_SELECTED_CATEGORIES' });
        }
      ).catch(
        error => store.dispatch({ type: 'CHANGE_TASK_ERROR', error })
      );
      return state;
    
    case 'TOGGLE_EDITOR':
      return {
        ...state,
        editoropen: !state.editoropen
      };
    
    case 'SET_CATEGORIES':
      tsclient.putUserCategories(action.categories).then(
        result => store.dispatch({ type: 'FETCH_USER', saving: true })
      ).catch(
        error => store.dispatch({ type: 'SET_CATEGORIES_ERROR', error })
      );
      return {
        ...state,
        saving: true
      };

    case 'EDIT_CATEGORY':
      const usercategories = Array.from(state.usercategories);
      let cat = usercategories[action.idx];
      if (!cat)
      {
        cat = { category: { name: action.name } };
      }
      else
      {
        cat.category.name = action.name;
      }
      usercategories[action.idx] = cat;
      return {
        ...state,
        usercategories
      }


    default:
      return state;
  }
}

const middleware = applyMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(appReducer, composeEnhancers(middleware));


const ErrorNotice = connect(
  state => ({
    error: state.error
  })
)(class extends React.Component {
  render()
  {
    const { error } = this.props;
    if (error)
    {
      return <div><strong>Error: {error.message}</strong></div>;
    }
    else
    {
      return <div />;
    }
  }
});

const timeformatter = new Intl.DateTimeFormat(
  'en-GB',
  {
    'hour': '2-digit',
    'minute': '2-digit'
  }
).format;

const StatusDisplay = connect(
  state => ({
    lastitem: state.lastitem,
    currenttime: state.currenttime,
    noitemloaded: state.noitemloaded
  })
)(class extends React.Component
{
  render()
  {
    let { lastitem, noitemloaded } = this.props;
    const currenttime = this.props.currenttime;
    let categorytext = "", elapsedtext = "", timetext = timeformatter(currenttime);

    if (noitemloaded)
    {
      categorytext = "Loading...";
    }
    else if (lastitem)
    {
      const lastitemdate = new Date(lastitem.time_started);
      let lastdeltaseconds = (currenttime - lastitemdate) / 1000;
      let lastdelta = moment.duration(
        lastdeltaseconds,
        'seconds'
      ).format('hh:mm');

      if (lastdeltaseconds < 3600)
      {
        lastdelta = "00:" + lastdelta;
      }

      // idle time is shown negative
      if (lastitem.isIdle)
      {
        lastdelta = '-' + lastdelta;
      }
      categorytext = lastitem.isIdle ? "" : lastitem.categories.join(' ');
      elapsedtext = lastdelta;
    }

    return (
      <div className="status-display">
        <div className="current-task-categories">{ categorytext }</div>
        <div className="status-times">
          <div className="current-task-elapsed">{ elapsedtext }</div>
          <div className="current-time">{ timetext }</div>
        </div>
      </div>
    );
  }
});

const CategoryButtons = connect(
  state => ({
    usercategories: state.usercategories.filter(c => !!c).map(c => c.category),
    userloading: state.userloading,
    selectedcategories: state.selectedcategories || new Set()
  })
)(class extends React.Component
{
  onClick(evt)
  {
    const category = evt.target.innerHTML.trim();
    this.props.dispatch({ type: 'TOGGLE_SELECTED_CATEGORY', category });
    evt.target.blur();
  }

  render()
  {
    const { usercategories, userloading, selectedcategories } = this.props;
    return (
      <div className="category-button-container">
      {
        userloading ? <button disabled className="category-button-loader">Loading...</button> : null
      }
      {
        usercategories.map(
          (category, i) => {
            const selectedclass = selectedcategories.has(category.name) ? " selected" : "";
            return (
              <button
                className={"category-button" + selectedclass}
                key={i}
                onClick={this.onClick.bind(this)}
              >
                {category.name}
              </button>
            )
          }
        )
      }
      </div>
    );
  }
});

const ActionButton = connect(
  state => ({
    lastitem: state.lastitem,
    selectedcategories: state.selectedcategories || new Set()
  })
)(class extends React.Component
{
  onClick(evt)
  {
    let categories = [];
    if (this.props.selectedcategories.size > 0)
    {
      categories = Array.from(this.props.selectedcategories);
    }
    else if (!this.props.lastitem.isIdle)
    {
      categories = ['END'];
    }
    if (categories.length)
    {
      this.props.dispatch({ type: 'CHANGE_TASK', categories });      
    }
    evt.target.blur();
  }

  render()
  {
    const hasselection = this.props.selectedcategories.size > 0;
    const enabled = !this.props.lastitem.isIdle || hasselection;
    const gc = this.props.lastitem.isIdle || hasselection;
    // console.log('action button render, gc=', gc);
    const text = enabled ? (gc ? 'GO' : 'STOP') : '';
    const cn = enabled ? (gc ? 'accept' : 'cancel') : 'standby';
    return (
      <div className="action-button-container">
        <button
          className={"action-button " + cn}
          onClick={this.onClick.bind(this)}
          disabled={!enabled}
        >
          {text}
        </button>
      </div>
    );
  }
});

const indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

const CategoriesEditor = connect(
  state => ({
    usercategories: state.usercategories,
    editoropen: state.editoropen
  })
)(class extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state = { names: indices.map(i => '') };
  }

  toggle(evt)
  {
    this.props.dispatch({ type: 'TOGGLE_EDITOR' });
    evt.target.blur();
  }

  save(evt)
  {
    const categories = this.state.names.map(c => c.trim()).filter(c => !!c);
    this.props.dispatch({ type: 'SET_CATEGORIES', categories });
    evt.target.blur();
  }

  static getDerivedStateFromProps(props, state)
  {
    const names = indices.map(i => {
      let cat = props.usercategories[i] || { category: { name: '' } };
      return cat.category.name;
    });
    return { names };
  }
  
  handleInput(evt)
  {
    this.props.dispatch({ type: 'EDIT_CATEGORY', idx: evt.target.name, name: evt.target.value });
  }

  render()
  {
    const { editoropen } = this.props;
    return (
      <div className="categories-editor">
        <button
          className="categories-editor-toggle"
          onClick={this.toggle.bind(this)}
        >
          Edit...
        </button>
        <div className={"category-inputs-container" + (editoropen ? ' open' : '')}>
          {this.state.names.map(
            (c, i) => <input
              name={i}
              key={i}
              type="text"
              value={c}
              onChange={this.handleInput.bind(this)}
              className="category-input"
              size="6"
            />
          )}
          <button className="categories-editor-save" onClick={this.save.bind(this)}>Save</button>
        </div>
      </div>
    );
  }
});

const UserInfo = connect(
  state => ({
    user: state.user
  })
)(class extends React.Component
{
  render()
  {
    const { user } = this.props;
    return (
      <div className="user-info">
        Logged in as { user.email }
      </div>
    );
  }
});

class App extends React.Component
{
  render()
  {
    return (
      <div className="the-task-switch">
        <div className="title">THE TASK SWITCH</div>
        <StatusDisplay />
        <ActionButton />
        <CategoryButtons />
        <CategoriesEditor />
        <UserInfo />
        <ErrorNotice />
      </div>
    );
  }
}
const ConnectedApp = connect(
)(App);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedApp />
  </Provider>,
  document.getElementById('root')
);

store.dispatch({ type: 'FETCH_USER' });
store.dispatch({ type: 'FETCH_LAST_ITEM' });

setInterval(
  () => {
    store.dispatch({ type: 'SET_TIME', time: new Date() });
  },
  5000
);

setInterval(
  () => {
    store.dispatch({ type: 'FETCH_LAST_ITEM' });
  },
  30000
);