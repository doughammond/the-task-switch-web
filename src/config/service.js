// @flow

let cfg = {};


if (process.env.NODE_ENV === 'development') {
  // when running locally under webpack dev server
  cfg = {
    "host": "localhost",
    "port": 3000,
    "path": "/api/v1",
    "secure": false,
    "use_token": false  
  };
}

if (process.env.NODE_ENV === 'production') {
  // when running fully in production
  cfg = {
    "host": "tts.hamsterfight.co.uk",
    "port": 443,
    "path": "/api/v1",
    "secure": true,
  };
}


export const config = cfg;
