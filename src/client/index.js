// @flow

import moment from 'moment';
import Promise from 'bluebird';
import request from 'superagent';

import { config } from '../config/service';

const proto = config.secure ? 'https' : 'http';
const url = `${proto}://${config.host}:${config.port}${config.path}/`;


// REST API style
const api_handler = function(token, resource, method, args={}) {
  return token.then(
    (t) => new Promise(
      (resolve, reject) => {
        let req = request[method](url + resource);
        if (config.user && config.pass) {
          req = req.auth(config.user, config.pass);
        } else if (t) {
          req = req.set('Authentication-Token', t);
        }
        req.send({ ...args })
           .end(
             (err, res) => {
               if (err) {
                 reject(err);
               } else {
                 resolve(res.body)
               }
             }
           )
      }
    )
  );
}


type _QueryResults = Array<any>;
type _PromiseQueryResults = Promise<_QueryResults>;

const datetimeformatterObject = new Intl.DateTimeFormat(
  'en-GB',
  {
    'year': 'numeric',
    'month': '2-digit',
    'day': '2-digit',
    'hour': '2-digit',
    'minute': '2-digit',
    'second': '2-digit'
  }
);

const datetimeformatter = (dt) => {
  const dtpa = datetimeformatterObject.formatToParts(dt);
  const dtp = dtpa.reduce(
    (p, c) => {
      p[c.type] = c.value;
      return p;
    },
    {}
  );
  return `${dtp.year}-${dtp.month}-${dtp.day} ${dtp.hour}:${dtp.minute}:${dtp.second}`;
}

export class TimesheetsClient {

  constructor() {
    if (config.use_token) {
      this.token = new Promise((resolve, reject) => {
        request
          .get(url + 'token')
          .end(
            (err, res) => {
              if (err) {
                reject(err);
              } else {
                resolve(res.text);
              }
            }
          );
      });
    } else {
      this.token = Promise.resolve();
    }
  }

  _flattenSheets(sheets) {
    return sheets.reduce(
      (items, sheet) => [...items, ...sheet.items],
      []
    );
  }

  _mapTimesheetItemsToModelData(items: _QueryResults): Object {
    return items.sort(
      (a, b) => Date.parse(a.time_started) - Date.parse(b.time_started)
    ).reduce(
      (data, item) => {
        const itemDate = moment(item.time_started).format('YYYY-MM-DD');
        if (!(itemDate in data)) {
          data[itemDate] = [];
        }
        data[itemDate].push([
          moment(item.time_started).toDate(),
          item.description,
          '', // classification
          item.categories
        ]);
        return data;
      },
      {}
    );
  }

  importTimesheet(date: Date, items: Array<any>): _PromiseQueryResults {
    throw new Error('not implemented')
  }

  getAllTimesheets(): _PromiseQueryResults {
    return api_handler(this.token, 'timesheets', 'get');
  }

  getTimesheetsByYear(year: number): _PromiseQueryResults {
    const reqdate = `${year}`;
    return api_handler(this.token, 'timesheets', 'get').filter(
      sheet => {
        const tmin = moment(sheet.min_item_time).format('YYYY');
        const tmax = moment(sheet.max_item_time).format('YYYY');
        const lb = tmin <= reqdate;
        const ub = reqdate <= tmax;
        // console.log(tmin, reqdate, tmax, lb, ub);
        return lb && ub;
      }
    ).map(
      sheet => api_handler(this.token, `timesheet/${sheet.id}`, 'get')
    ).then(
      sheets => this._flattenSheets(sheets)
    ).then(
      items => this._mapTimesheetItemsToModelData(items)
    ).tap(
      week_sheets => console.log('yearly sheets', week_sheets)
    );
  }

  getTimesheetsByQuarter(year: number, quarter: number): _PromiseQueryResults {
    const reqdate = `${year}-${quarter}`;
    return api_handler(this.token, 'timesheets', 'get').filter(
      sheet => {
        const tmin = moment(sheet.min_item_time).format('YYYY-Q');
        const tmax = moment(sheet.max_item_time).format('YYYY-Q');
        const lb = tmin <= reqdate;
        const ub = reqdate <= tmax;
        // console.log(tmin, reqdate, tmax, lb, ub);
        return lb && ub;
      }
    ).map(
      sheet => api_handler(this.token, `timesheet/${sheet.id}`, 'get')
    ).then(
      sheets => this._flattenSheets(sheets)
    ).then(
      items => this._mapTimesheetItemsToModelData(items)
    ).tap(
      week_sheets => console.log('quarterly sheets', week_sheets)
    );
  }

  getTimesheetsByWeek(year: number, week: number): _PromiseQueryResults {
    const reqdate = `${year}-${week}`;
    return api_handler(this.token, 'timesheets', 'get').filter(
      sheet => {
        const tmin = moment(sheet.min_item_time).format('GGGG-WW');
        const tmax = moment(sheet.max_item_time).format('GGGG-WW');
        const lb = tmin <= reqdate;
        const ub = reqdate <= tmax;
        // console.log(tmin, reqdate, tmax, lb, ub);
        return lb && ub;
      }
    ).map(
      sheet => api_handler(this.token, `timesheet/${sheet.id}`, 'get')
    ).then(
      sheets => this._flattenSheets(sheets)
    ).then(
      items => this._mapTimesheetItemsToModelData(items)
    ).tap(
      week_sheets => console.log('weekly sheets', week_sheets)
    );
  }

  getUser() {
    return api_handler(this.token, 'user', 'get');
  }

  getLastItem() {
    return api_handler(this.token, 'timesheet_item/latest', 'get');
  }

  putTimesheetItem(categories) {
    return api_handler(this.token, 'timesheet_item', 'put', {
      time_started: datetimeformatter(new Date()),
      categories
    });
  }

  putUserCategories(categories) {
    return api_handler(this.token, 'user/categories', 'put', {
      categories
    });
  }
};

export const Client = new TimesheetsClient();
